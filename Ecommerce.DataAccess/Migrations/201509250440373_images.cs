namespace Ecommerce.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class images : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Description", c => c.String());
            DropColumn("dbo.Products", "Descrpition");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Products", "Descrpition", c => c.String());
            DropColumn("dbo.Products", "Description");
        }
    }
}
